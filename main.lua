io.stdout:setvbuf('no')

-- love.graphics.setColor(1, 0, 0, 1) Red

-- Required for pixel art
love.graphics.setDefaultFilter("nearest")

-- ZeroBraneStudio
if arg[#arg] == "-debug" then require("mobdebug").start() end

if pcall(require, "lldebugger") then
    require("lldebugger").start()
end

DEBUG = true
GRID = true

local MOUSE = require("src/mouse")

-- local bg
local screenWidth
local screenHeight
local camera = { x = 0 ; y = 0 }
local border = false
local counter = 0

MARGIN = 20
SCROLLING = 192
OUTSIDE = 200
MAP = 36
TILESIZE = 32

function love.load()
    -- bg = love.graphics.newImage("img/island.png")

    screenWidth, screenHeight = love.graphics.getDimensions()
    -- -- Set camera in the middle
    -- camera = {
    --     x = screenWidth / 2 ;
    --     y = screenHeight / 2
    -- }

    MOUSE:load()
end

function love.update(dt)
    MOUSE:update()

    -- Move camera using mouse
    camera_mouse(dt)

    --Move camera using keyboard
    camera_keyboard(dt)
end

function love.draw()
    local map_x, map_y = 0, 0
    local generated = 0

    -- draw background
    for l = 1, MAP do
        for c = 1, MAP do
            pos_x = map_x - camera.x
            pos_y = map_y - camera.y

            if (c * TILESIZE > camera.x and pos_x < screenWidth) and (l * TILESIZE > camera.y and pos_y < screenHeight) then
                love.graphics.setColor(0, .41, 0.58, 1)
                love.graphics.rectangle("fill", pos_x, pos_y, TILESIZE, TILESIZE)

                if GRID == true then
                    love.graphics.setColor(0, 0, 0, 1)
                    love.graphics.rectangle("line", pos_x, pos_y, TILESIZE, TILESIZE)
                end

                generated = generated + 1
            end

            map_x = map_x + TILESIZE
        end

        map_y = map_y + TILESIZE
        map_x = 0
    end

    -- love.graphics.setColor(1, 1, 1, 1)

    --[[
        HUD

        - fixed on screen
        - can be moved
    ]]
    love.graphics.setColor(0.5, 0.5, 0.5,1)
    love.graphics.rectangle("fill", screenWidth - (128 + 50), 50, 128, 32)

    -- Debug information
    if (DEBUG) then
        love.graphics.setColor(1, 0, 0, 1)
        love.graphics.line(-OUTSIDE, MARGIN, screenWidth, MARGIN)
        love.graphics.line(-OUTSIDE, screenHeight - MARGIN, screenWidth, screenHeight - MARGIN)
        love.graphics.line(MARGIN, 0, MARGIN, screenHeight)
        love.graphics.line(screenWidth - MARGIN, 0, screenWidth - MARGIN, screenHeight)

        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.print("FPS: " .. love.timer.getFPS(), 25, 25)
        love.graphics.print("screen: " .. tostring(screenWidth) .. " x " .. tostring(screenHeight), 25, 41)
        love.graphics.print("camera: " .. tostring(math.floor(camera.x)) .. " x " .. tostring(math.floor(camera.y)) , 25, 57)
        love.graphics.print("mouse X: " .. mouse_x .. ", Y: " .. mouse_y, 25, 73)
        love.graphics.print("generated rectangles: " .. generated, 25, 105)

        if border == true then
            color = {1, 0, 0, 1}
        else
            color = {1, 1, 1, 1}
        end
        love.graphics.printf({ color, "border: " .. tostring(border), {1,1,1}, " / counter: " .. tostring(math.floor(counter)) }, 25, 89, 500)
    end

    love.graphics.setColor(0, 0, 0, 0.5)
    love.graphics.rectangle("fill", 0, screenHeight - 20, screenWidth, screenHeight - 20)
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.print("Debug mode: d / show grid: g / reset camera: r / escape: escape", screenWidth / 4, screenHeight - 20)

    MOUSE:draw()
end

function love.keypressed(key)
    if (DEBUG) then
	    print("Key pressed", key)
    end

    if key == "d" then
        DEBUG = not DEBUG
    end

    if key == "g" then
        GRID = not GRID
    end

 	if key == "escape" then
		love.event.quit()
		return
	end

    if key == "r" then
        camera.x = 0
        camera.y = 0
    end

    MOUSE:keypressed(key)
end

function love.mousepressed(x, y, button, istouch, presses)
    MOUSE:mousepressed(x, y, button, istouch, presses)
end

function love.wheelmoved(x, y)
    MOUSE:wheelmoved(x, y)    
end

function love.mousemoved(x, y, dx, dy)
    MOUSE:mousemoved(x, y, dx, dy)
end

function camera_mouse(dt)

    border = false

    mouse_x, mouse_y = love.mouse.getPosition()
    -- Move camera using mouse
    -- Up
    if mouse_y < MARGIN and mouse_y > 0 then
        border = true
        counter = counter + 1 * dt
        if counter >= 0.5 then
            if camera.y > -OUTSIDE then
                camera.y = camera.y - SCROLLING * dt
            end
        end
    end
    -- Right
    if mouse_x > screenWidth - MARGIN and mouse_x < (screenWidth - 2) then
        border = true
        counter = counter + 1 * dt
        if counter >= 0.5 then
            if (camera.x + screenWidth) < (MAP * TILESIZE + OUTSIDE) then
                camera.x = camera.x + SCROLLING * dt
            end
        end
    end
    -- Down
    if mouse_y > screenHeight - MARGIN and mouse_y < (screenHeight - 2) then
        border = true
        counter = counter + 1 * dt
        if counter >= 0.5 then
            if (camera.y + screenHeight) < (MAP * TILESIZE + OUTSIDE) then
                camera.y = camera.y + SCROLLING * dt
            end
        end
    end
    -- Left
    if mouse_x < MARGIN and mouse_x > 0 then
        border = true
        counter = counter + 1 * dt
        if counter >= 0.5 then
            if camera.x > -OUTSIDE then
                camera.x = camera.x - SCROLLING * dt
            end
        end
    end

    if border == false then
        counter = 0
    end

end

function camera_keyboard(dt)
    --Move camera using keyboard
    if love.keyboard.isDown("up") then
        if camera.y > -SCROLLING then
            camera.y = camera.y - SCROLLING * dt
        end
    end
    if love.keyboard.isDown("right") then
        if (camera.x + screenWidth) < (MAP * TILESIZE + 200) then
            camera.x = camera.x + SCROLLING * dt
        end
    end
    if love.keyboard.isDown("down") then
        if (camera.y + screenHeight) < (MAP * TILESIZE + 200) then
            camera.y = camera.y + SCROLLING * dt
        end
    end
    if love.keyboard.isDown("left") then
        if camera.x > -SCROLLING then
            camera.x = camera.x - SCROLLING * dt
        end
    end
end