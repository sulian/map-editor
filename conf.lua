function love.conf (t)
    t.window.title = "WaW - Map editor"
    t.window.width = 1024
    t.window.height = 768

    t.console = true
end